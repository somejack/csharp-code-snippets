# Change Log

## [0.0.1] (November 11, 2018) - Initial release

### Added

- class
- ctor
- cw
- do
- else
- enum
- fact
- for
- foreach
- guid
- if
- ifd
- interface
- mbox
- namespace
- prop
- propfull
- region
- svm
- switch
- try
- tryf
- using
- while
